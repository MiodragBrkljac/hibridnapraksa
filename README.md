# Hybrid Library REST API web application

Implement Hybrid Library sample application with following features:

* Application users should be populated automatically on application startup
* Application users can have different role
* User password should be hashed
* Admins can fetch registered user(s) (password should not be exposed)
* Admins can manage books (create, update, delete), whereas both admins and regular users can fetch books
* Book cannot be deleted if there are rented copies
* Admins and regular users can rent/return books if there are available copies
* Book rent period should be configurable
* Admins can fetch most rented book
* Admins can fetch overdue book returns
* All implemented endpoints should be secured
* Use Swagger to document endpoints
* Use Logback to define configurable log files location and time & size based rolling policy

### Resources

* [Spring Boot](https://spring.io/projects/spring-boot)
* [Spring Security](https://spring.io/projects/spring-security)
* [Maven](https://maven.apache.org/)
* [H2 database](https://www.h2database.com/html/main.html)
* [Lombok](https://projectlombok.org/)
* [Logback](https://logback.qos.ch/)
* [Swagger](https://swagger.io/)

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.1.8.RELEASE/maven-plugin/)
* [Spring Security](https://docs.spring.io/spring-boot/docs/{bootVersion}/reference/htmlsingle/#boot-features-security)

* [Securing a Web Application](https://spring.io/guides/gs/securing-web/)
* [Spring Boot and OAuth2](https://spring.io/guides/tutorials/spring-boot-oauth2/)
* [Authenticating a User with LDAP](https://spring.io/guides/gs/authenticating-ldap/)



To run docker image of this app run: 

docker login registry.gitlab.com

docker build -t registry.gitlab.com/miodragbrkljac/hibridnapraksaorganizacijarada/library-backend .

docker push registry.gitlab.com/miodragbrkljac/hibridnapraksaorganizacijarada/library-backend

docker run -p 8080:8080 registry.gitlab.com/miodragbrkljac/hibridnapraksaorganizacijarada/library-backend
